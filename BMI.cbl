       IDENTIFICATION DIVISION.
       PROGRAM-ID. BMI.
       AUTHOR. MUTITA.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  WEIGHT          PIC 9(3)V99 VALUE 0.
       01  H               PIC 9(3)V99 VALUE 0.
       01  BMI             PIC 9(4)V99 VALUE 0.

       PROCEDURE DIVISION.
           DISPLAY "Enter your weight in kilograms: " WITH NO ADVANCING.
           ACCEPT WEIGHT.

           DISPLAY "Enter your height in centimeters: " WITH NO 
           ADVANCING.
           ACCEPT H.

           COMPUTE BMI = WEIGHT / ( (H / 100 ) * (H / 100 )).

           DISPLAY "Your BMI is: " BMI .

           EVALUATE TRUE
           WHEN BMI < 18.5
             DISPLAY "Underweight"
           WHEN BMI >= 18.5 AND BMI < 24.9
             DISPLAY "Normal weight"
           WHEN BMI >= 25 AND BMI < 29.9
             DISPLAY "Overweight"
           WHEN BMI >= 30
             DISPLAY "Obese"
           END-EVALUATE.
           GOBACK
       .

 